# README #
Projeto SOS Mascote

### Para execução é necessária instalação dos seguintes programas: ###

* Python 3
    * PIP
    * VirtualEnv

## Clonar o projeto ##
* git clone https://dusakai@bitbucket.org/dusakai/sosmascote.git

## Primeira execução ##
* crie um ambiente virtual

```
virtualenv -p python3 venv
```
* habilite o anbiente virtual

```
source venv/bin/activate
```

* instale as dependências

* para isso, entre na pasta do projeto e execute:

```
pip install -r requirements.txt
```

### crie um usuário ###
```
python manage.py createsuperuser
```

### Execute o servidor ###
```
python manage.py runserver
```
