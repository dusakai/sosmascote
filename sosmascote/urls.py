from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    # urls descentralizadas
    path('api/', include('local.urls')),
    path('api/', include('pessoa.urls')),
    path('api/', include('mascote.urls')),
]
