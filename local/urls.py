from django.urls import path, include
from local.views import ListaLocal, DetalheLocal

urlpatterns = [
    path('locais/', ListaLocal.as_view(), name="lista_local"),
    path('locais/<int:pk>/', DetalheLocal.as_view(), name="detalhe_local")
]
