from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from rest_framework import status

from .models import Local
from .serializers import LocalSerializer


class ListaLocal(APIView):
    def get(self, request):
        locais = Local.objects.all()
        dado = LocalSerializer(locais, many=True).data
        return Response(dado)

    def post(self, request):
        serializer = LocalSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalheLocal(APIView):

    def get_object(self, pk):
        try:
            return Local.objects.get(pk=pk)
        except Local.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        local = self.get_object(pk)
        dado = LocalSerializer(local)
        return Response(dado.data)

    def put(self, request, pk):
        local = self.get_object(pk)
        serializer = LocalSerializer(local, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        local = self.get_object(pk)
        local.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
