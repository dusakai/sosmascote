from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from rest_framework import status

from .models import Mascote, MascoteEncontrado, MascotePerdido
from .serializers import MascoteSerializer, MascoteEncontradoSerializer, MascotePerdidoSerializer


class ListaMascote(APIView):
    def get(self, request):
        mascotes = Mascote.objects.all()
        dado = MascoteSerializer(mascotes, many=True).data
        return Response(dado)

    def post(self, request):
        serializer = MascoteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalheMascote(APIView):

    def get_object(self, pk):
        try:
            return Mascote.objects.get(pk=pk)
        except Mascote.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        mascote = self.get_object(pk)
        dado = MascoteSerializer(mascote)
        return Response(dado.data)

    def put(self, request, pk):
        mascote = self.get_object(pk)
        serializer = MascoteSerializer(mascote, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        mascote = self.get_object(pk)
        mascote.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListaMascotePerdido(APIView):
    def get(self, request):
        mascotes_perdidos = MascotePerdido.objects.all()
        dado = MascotePerdidoSerializer(mascotes_perdidos, many=True).data
        return Response(dado)

    def post(self, request):
        serializer = MascotePerdidoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalheMascotePerdido(APIView):

    def get_object(self, pk):
        try:
            return MascotePerdido.objects.get(pk=pk)
        except MascotePerdido.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        mascote_perdido = self.get_object(pk)
        dado = MascotePerdidoSerializer(mascote_perdido)
        return Response(dado.data)

    def put(self, request, pk):
        mascote_perdido = self.get_object(pk)
        serializer = MascotePerdidoSerializer(mascote_perdido, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        mascote_perdido = self.get_object(pk)
        mascote_perdido.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListaMascoteEncontrado(APIView):
    def get(self, request):
        mascotes_encontrados = MascoteEncontrado.objects.all()
        dado = MascoteEncontradoSerializer(mascotes_encontrados, many=True).data
        return Response(dado)

    def post(self, request):
        serializer = MascoteEncontradoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            if serializer.data['mascote']:
                try:
                    mascote = Mascote.objects.get(id=serializer.data['mascote']['id'])
                    if mascote.mascoteperdido_set.filter(encontrado=False).exists():
                        perdido = mascote.mascoteperdido_set.filter(encontrado=False).last()
                        perdido.encontrado = True
                        perdido.save()
                except Exception as e:
                    print(e)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalheMascoteEncontrado(APIView):

    def get_object(self, pk):
        try:
            return MascoteEncontrado.objects.get(pk=pk)
        except MascoteEncontrado.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        mascote_encontrado = self.get_object(pk)
        dado = MascoteEncontradoSerializer(mascote_encontrado)
        return Response(dado.data)

    def put(self, request, pk):
        mascote_encontrado = self.get_object(pk)
        serializer = MascoteEncontradoSerializer(mascote_encontrado, data=request.data)
        if serializer.is_valid():
            serializer.save()
            if serializer.data['mascote']:
                try:
                    mascote = Mascote.objects.get(id=serializer.data['mascote']['id'])
                    if mascote.mascoteperdido_set.filter(encontrado=False).exists():
                        perdido = mascote.mascoteperdido_set.filter(encontrado=False).last()
                        perdido.encontrado = True
                        perdido.save()
                except Exception as e:
                    print(e)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        mascote_encontrado = self.get_object(pk)
        mascote_encontrado.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
