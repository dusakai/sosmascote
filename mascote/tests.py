from django.test import TestCase
from django.utils import timezone

from local import constants
from local.models import Local
from pessoa.models import Pessoa
from mascote.models import Mascote, MascotePerdido, MascoteEncontrado


class MascoteTestCase(TestCase):
    '''  Teste unitario da classe Mascote '''

    def setUp(self):
        celso_ferraz = Local.objects.create(
            logradouro='Rua Prof. Celso Ferraz de Camargo',
            numero='350',
            bairro='Cidade Universitária',
            complemento='Bloco B',
            cidade='Campinas',
            estado=constants.SP,
            cep='13083-200',
            geocode='-22.803997, -47.064340'
        )
        horacio = Pessoa.objects.create(
            nome='Horacio',
            cpf='414.193.320-15',
            data_nascimento='1985-12-30',
            fone='19999666333',
            local=celso_ferraz,
        )
        Mascote.objects.create(
            dono=horacio,
            nome='Espuleta',
            data_nascimento='2017-03-06',
            raca='Braco Alemão',
            porte='Grande',
            cor='Fígado ruão',
            bravo=False,
        )

    def test_get(self):
        espuleta = Mascote.objects.get(nome='Espuleta', raca='Braco Alemão')
        self.assertIsInstance(espuleta, Mascote)

    def test_idade(self):
        espuleta = Mascote.objects.get(nome='Espuleta', raca='Braco Alemão')
        self.assertEqual(espuleta.idade, 2)


class MascotePerdidoTestCase(TestCase):
    '''  Teste unitario da classe MascotePerdido '''

    def setUp(self):
        celso_ferraz = Local.objects.create(
            logradouro='Rua Prof. Celso Ferraz de Camargo',
            numero='350',
            bairro='Cidade Universitária',
            complemento='Bloco B',
            cidade='Campinas',
            estado=constants.SP,
            cep='13083-200',
            geocode='-22.803997, -47.064340'
        )
        albiono_jose = Local.objects.create(
            logradouro='Av. Albino José B. Oliverira',
            numero='2151',
            bairro='Barão Geraldo',
            cidade='Campinas',
            estado=constants.SP,
            cep='13084-510',
            geocode='-22.83399, -47.07935'
        )
        horacio = Pessoa.objects.create(
            nome='Horacio',
            cpf='414.193.320-15',
            data_nascimento='1985-12-30',
            fone='19999666333',
            local=celso_ferraz,
        )
        espuleta = Mascote.objects.create(
            dono=horacio,
            nome='Espuleta',
            data_nascimento='2017-03-06',
            raca='Braco Alemão',
            porte='Grande',
            cor='Fígado ruão',
            bravo=False,
        )
        espuleta.mascoteperdido_set.create(
            ultimo_local=albiono_jose,
            encontrado=False,
        )

    def test_get(self):
        espuleta_perdida = MascotePerdido.objects.get(mascote__nome='Espuleta', mascote__raca='Braco Alemão')
        espuleta = Mascote.objects.get(nome='Espuleta', raca='Braco Alemão')
        self.assertIsInstance(espuleta, Mascote)
        self.assertIsInstance(espuleta_perdida, MascotePerdido)


class MascoteEncontradoTestCase(TestCase):
    '''  Teste unitario da classe MascoteEncontrado '''

    def setUp(self):
        celso_ferraz = Local.objects.create(
            logradouro='Rua Prof. Celso Ferraz de Camargo',
            numero='350',
            bairro='Cidade Universitária',
            complemento='Bloco B',
            cidade='Campinas',
            estado=constants.SP,
            cep='13083-200',
            geocode='-22.803997, -47.064340'
        )
        albiono_jose = Local.objects.create(
            logradouro='Av. Albino José B. Oliverira',
            numero='2151',
            bairro='Barão Geraldo',
            cidade='Campinas',
            estado=constants.SP,
            cep='13084-510',
            geocode='-22.83399, -47.07935'
        )
        horacio = Pessoa.objects.create(
            nome='Horacio',
            cpf='414.193.320-15',
            data_nascimento='1985-12-30',
            fone='19999666333',
            local=celso_ferraz,
        )
        espuleta = Mascote.objects.create(
            dono=horacio,
            nome='Espuleta',
            data_nascimento='2017-03-06',
            raca='Braco Alemão',
            porte='Grande',
            cor='Fígado ruão',
            bravo=False,
        )
        espuleta_perdida = espuleta.mascoteperdido_set.create(
            ultimo_local=albiono_jose,
            encontrado=False,
        )

        mascote_perdido = True if espuleta_perdida else False

        espuleta.mascoteencontrado_set.create(
            local_achado=albiono_jose,
            mascote_estava_perdido=mascote_perdido,
            data_achado=timezone.now(),
        )

    def test_get(self):
        espuleta = Mascote.objects.get(nome='Espuleta', raca='Braco Alemão')
        espuleta_perdida = MascotePerdido.objects.get(mascote__nome='Espuleta', mascote__raca='Braco Alemão')
        espuleta_encontrada = MascoteEncontrado.objects.get(mascote__nome='Espuleta', mascote__raca='Braco Alemão')
        self.assertIsInstance(espuleta, Mascote)
        self.assertIsInstance(espuleta_perdida, MascotePerdido)
        self.assertIsInstance(espuleta_encontrada, MascoteEncontrado)

    def test_perdido_quando_encontrado_muda_status(self):
        espuleta_perdida = MascotePerdido.objects.get(mascote__nome='Espuleta', mascote__raca='Braco Alemão')
        self.assertEqual(espuleta_perdida.encontrado, True)
