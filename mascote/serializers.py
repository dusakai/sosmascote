from rest_framework import serializers
from local.serializers import LocalSerializer
from local.models import Local
from pessoa.serializers import PessoaSerializer
from pessoa.models import Pessoa
from .models import Mascote, MascotePerdido, MascoteEncontrado


class MascoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mascote
        fields = '__all__'

    def is_valid(self, raise_exception=False):
        self.dono = self.initial_data.pop('dono', None)

        return super(MascoteSerializer, self).is_valid(raise_exception)

    def create(self, validated_data):
        mascote = super(MascoteSerializer, self).create(validated_data)
        try:
            if self.dono is not None:
                dono = Pessoa.objects.get(id=self.dono['id'])
                mascote.dono = dono

            mascote.save(update_fields=['dono'])
            return mascote
        except Exception as e:
            mascote.delete()
            return mascote

    def update(self, mascote, validated_data):
        mascote = super(MascoteSerializer, self).update(mascote, validated_data)
        try:
            if self.dono is not None:
                dono = Pessoa.objects.get(id=self.dono['id'])
                mascote.dono = dono
            mascote.save(update_fields=['dono'])
            return mascote
        except Exception as e:
            mascote.delete()
            return mascote

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['dono'] = '' if instance.dono == "" or instance.dono == None else PessoaSerializer(instance.dono).data

        return response


class MascotePerdidoSerializer(serializers.ModelSerializer):

    class Meta:
        model = MascotePerdido
        fields = '__all__'

    def is_valid(self, raise_exception=False):
        self.mascote = self.initial_data.pop('mascote', None)
        self.ultimo_local = self.initial_data.pop('ultimo_local', None)
        return super(MascotePerdidoSerializer, self).is_valid(raise_exception)

    def create(self, validated_data):
        mascote_perdido = super(MascotePerdidoSerializer, self).create(validated_data)
        try:
            if self.mascote is not None:
                mascote = Mascote.objects.get(id=self.mascote['id'])
                mascote_perdido.mascote = mascote
            if self.ultimo_local is not None:
                ultimo_local = Local.objects.get(id=self.ultimo_local['id'])
                mascote_perdido.ultimo_local = ultimo_local

            mascote_perdido.save(update_fields=['mascote', 'ultimo_local'])
            return mascote_perdido
        except Exception as e:
            mascote_perdido.delete()
            return mascote_perdido

    def update(self, mascote, validated_data):
        mascote_perdido = super(MascotePerdidoSerializer, self).update(mascote, validated_data)
        try:
            if self.mascote is not None:
                mascote = Mascote.objects.get(id=self.mascote['id'])
                mascote_perdido.mascote = mascote
            if self.ultimo_local is not None:
                ultimo_local = Local.objects.get(id=self.ultimo_local['id'])
                mascote_perdido.ultimo_local = ultimo_local
            mascote_perdido.save(update_fields=['mascote', 'ultimo_local'])
            return mascote_perdido
        except Exception as e:
            mascote_perdido.delete()
            return mascote_perdido

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['mascote'] = '' if instance.mascote == "" or instance.mascote == None else MascoteSerializer(instance.mascote).data
        response['ultimo_local'] = '' if instance.ultimo_local == "" or instance.ultimo_local == None else LocalSerializer(instance.ultimo_local).data
        return response



class MascoteEncontradoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MascoteEncontrado
        fields = '__all__'

    def is_valid(self, raise_exception=False):
        self.mascote = self.initial_data.pop('mascote', None)
        self.local_achado = self.initial_data.pop('local_achado', None)
        return super(MascoteEncontradoSerializer, self).is_valid(raise_exception)

    def create(self, validated_data):
        mascote_encontrado = super(MascoteEncontradoSerializer, self).create(validated_data)
        try:
            if self.mascote is not None:
                mascote = Mascote.objects.get(id=self.mascote['id'])
                mascote_encontrado.mascote = mascote
            if self.local_achado is not None:
                local_achado = Local.objects.get(id=self.local_achado['id'])
                mascote_encontrado.local_achado = local_achado
            mascote_encontrado.save(update_fields=['mascote', 'local_achado'])
            return mascote_encontrado
        except Exception as e:
            mascote_encontrado.delete()
            return mascote_encontrado

    def update(self, mascote, validated_data):
        mascote_encontrado = super(MascoteEncontradoSerializer, self).update(mascote, validated_data)
        try:
            if self.mascote is not None:
                mascote = Mascote.objects.get(id=self.mascote['id'])
                mascote_encontrado.mascote = mascote
            if self.local_achado is not None:
                local_achado = Local.objects.get(id=self.local_achado['id'])
                mascote_encontrado.local_achado = local_achado
            mascote_encontrado.save(update_fields=['mascote', 'local_achado'])
            return mascote_encontrado
        except Exception as e:
            mascote_encontrado.delete()
            return mascote_encontrado

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['mascote'] = '' if instance.mascote == "" or instance.mascote == None else MascoteSerializer(instance.mascote).data
        response['local_achado'] = '' if instance.local_achado == "" or instance.local_achado == None else LocalSerializer(instance.local_achado).data
        return response
