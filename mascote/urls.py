from django.urls import path, include
from mascote.views import ListaMascote, DetalheMascote, ListaMascotePerdido, DetalheMascotePerdido, \
    ListaMascoteEncontrado, DetalheMascoteEncontrado

urlpatterns = [
    path('mascotes/', ListaMascote.as_view(), name="lista_mascote"),
    path('mascotes/<int:pk>/', DetalheMascote.as_view(), name="detalhe_mascote"),
    path('mascotes_perdidos/', ListaMascotePerdido.as_view(), name="lista_mascote_perdido"),
    path('mascotes_perdidos/<int:pk>/', DetalheMascotePerdido.as_view(), name="detalhe_mascote_perdido"),
    path('mascotes_encontrados/', ListaMascoteEncontrado.as_view(), name="lista_mascote_encontrado"),
    path('mascotes_encontrados/<int:pk>/', DetalheMascoteEncontrado.as_view(), name="detalhe_mascote_encontrado")
]
