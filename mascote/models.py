import datetime
from datetime import date

from django.db import models
from django.utils import timezone

from local.models import Local
from pessoa.models import Pessoa

from . import constants


class Mascote(models.Model):
    dono = models.ForeignKey(Pessoa, verbose_name="pessoa", on_delete=models.CASCADE, null=True, blank=True)
    nome = models.CharField("nome", max_length=64, null=True, blank=True)
    data_nascimento = models.DateField("data de nascimento", null=True, blank=True)
    raca = models.CharField("raça", max_length=64, null=True, blank=True)
    porte = models.CharField("porte", max_length=16, choices=constants.PORTE, default=constants.MEDIO)
    cor = models.CharField("nome", max_length=64, null=True, blank=True)
    bravo = models.BooleanField("bravo", default=False)
    # status control
    criado_em = models.DateTimeField(editable=False)
    editado_em = models.DateTimeField("última atualização", null=True, blank=True)


    class Meta:
        ordering = ('nome',)
        verbose_name = 'mascote'
        verbose_name_plural = 'mascote'

    def __str__(self):
        return self.nome

    def save(self, *args, **kwargs):
        # Atualizar datas criacao edicao
        if not self.criado_em:
            self.criado_em = timezone.now()
        self.editado_em = timezone.now()
        return super(Mascote, self).save(*args, **kwargs)

    @property
    def idade(self):
        if self.data_nascimento:
            hoje = date.today()
            return hoje.year - self.data_nascimento.year - ((hoje.month, hoje.day) < (self.data_nascimento.month, self.data_nascimento.day))
        return None


class MascotePerdido(models.Model):
    mascote = models.ForeignKey(Mascote, verbose_name="macote", on_delete=models.CASCADE, null=True, blank=True)
    data_sumico = models.DateTimeField("data do desaparecimento", null=True, blank=True)
    ultimo_local = models.ForeignKey(Local, verbose_name="ultimo local", on_delete=models.SET_NULL, null=True, blank=True)
    encontrado = models.BooleanField("encontrado", default=False)
    # status control
    criado_em = models.DateTimeField(editable=False)
    editado_em = models.DateTimeField("última atualização", null=True, blank=True)

    class Meta:
        ordering = ('-criado_em',)
        verbose_name = 'mascote perdido'
        verbose_name_plural = 'mascotes perdidos'

    def __str__(self):
        return 'SOS Mascote perdido: ' + self.mascote.nome

    def save(self, *args, **kwargs):
        # Atualizar datas criacao edicao
        if not self.criado_em:
            self.criado_em = timezone.now()
        self.editado_em = timezone.now()
        return super(MascotePerdido, self).save(*args, **kwargs)


class MascoteEncontrado(models.Model):
    mascote = models.ForeignKey(Mascote, verbose_name="macote", on_delete=models.CASCADE, null=True, blank=True)
    mascote_estava_perdido = models.BooleanField("sumico informado", default=True, null=True, blank=True)
    data_achado = models.DateTimeField("data do desaparecimento", null=True, blank=True)
    local_achado = models.ForeignKey(Local, verbose_name="ultimo local", on_delete=models.SET_NULL, null=True, blank=True)
    # status control
    criado_em = models.DateTimeField(editable=False)
    editado_em = models.DateTimeField("última atualização", null=True, blank=True)

    class Meta:
        ordering = ('-criado_em',)
        verbose_name = 'mascote encontrado'
        verbose_name_plural = 'mascotes encontrado'

    def __str__(self):
        return 'SOS Mascote achado: ' + self.mascote.nome

    def save(self, *args, **kwargs):
        # Atualizar datas criacao edicao
        if not self.criado_em:
            self.criado_em = timezone.now()
        self.editado_em = timezone.now()
        return super(MascoteEncontrado, self).save(*args, **kwargs)
