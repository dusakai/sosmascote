import datetime
from datetime import date

import re
from django.db import models
from django.utils import timezone

from local.models import Local


class Pessoa(models.Model):
    ''' Modelo que representa a entidade pessoa. '''
    # identification
    nome = models.CharField("nome", max_length=50, null=True, blank=True)
    cpf = models.CharField("CPF", unique=True, db_index=True, max_length=15)
    data_nascimento = models.DateField('data de nascimento', null=True, blank=True)
    # contact
    fone = models.CharField("fone", max_length=11, null=True, blank=True)
    # address
    local = models.ForeignKey(Local, verbose_name="endereço", on_delete=models.CASCADE, null=True, blank=True)
    # status control
    criado_em = models.DateTimeField(editable=False)
    editado_em = models.DateTimeField("última atualização", null=True, blank=True)

    class Meta:
        ordering = ('nome',)
        verbose_name = 'pessoa'
        verbose_name_plural = 'pessoas'

    def __str__(self):
        return self.nome

    def save(self, *args, **kwargs):
        self.cpf = self.cpf_digitos()
        # Atualizar datas criacao edicao
        if not self.criado_em:
            self.criado_em = timezone.now()
        self.editado_em = timezone.now()
        return super(Pessoa, self).save(*args, **kwargs)

    def cpf_digitos(self):
        '''
        Retorna cpf sem formatação (somente números)
        :return: cpf
        '''
        if self.cpf:
            return re.sub('[./-]', '', self.cpf)
        else:
            return ''

    def cpf_formatado(self):
        '''
        Método que retorna o cpf formatodo
        :return: cpf
        '''
        if self.cpf:
            if len(self.cpf) == 10:
                return "{0}.{1}.{2}-{3}".format(self.cpf[:2], self.cpf[2:5], self.cpf[5:8], self.cpf[8:])
            return "{0}.{1}.{2}-{3}".format(self.cpf[:3], self.cpf[3:6], self.cpf[6:9], self.cpf[9:])
        else:
            return ''

    @property
    def idade(self):
        if self.data_nascimento:
            hoje = date.today()
            return hoje.year - self.data_nascimento.year - ((hoje.month, hoje.day) < (self.data_nascimento.month, self.data_nascimento.day))
        return None
