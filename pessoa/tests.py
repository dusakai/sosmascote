from django.test import TestCase

from local import constants
from local.models import Local
from pessoa.models import Pessoa


class PessoaTestCase(TestCase):
    '''  Teste unitario da classe Pessoa '''

    def setUp(self):
        celso_ferraz = Local.objects.create(
            logradouro='Rua Prof. Celso Ferraz de Camargo',
            numero='350',
            bairro='Cidade Universitária',
            complemento='Bloco B',
            cidade='Campinas',
            estado=constants.SP,
            cep='13083-200',
            geocode='-22.803997, -47.064340'
        )
        Pessoa.objects.create(
            nome='Horacio',
            cpf='414.193.320-15',
            data_nascimento='1985-12-30',
            fone='19999666333',
            local=celso_ferraz,
        )

    def test_get(self):
        horacio = Pessoa.objects.get(cpf='41419332015')
        self.assertIsInstance(horacio, Pessoa)

    def test_cpf_digitos(self):
        horacio = Pessoa.objects.get(cpf='41419332015')
        self.assertEqual(horacio.cpf_digitos(), '41419332015')

    def test_cpf_formatado(self):
        horacio = Pessoa.objects.get(cpf='41419332015')
        self.assertEqual(horacio.cpf_formatado(), '414.193.320-15')

    def test_idade(self):
        horacio = Pessoa.objects.get(cpf='41419332015')
        self.assertEqual(horacio.idade, 33)
