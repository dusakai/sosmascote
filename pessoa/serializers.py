from rest_framework import serializers
from local.serializers import LocalSerializer
from local.models import Local
from .models import Pessoa


class PessoaSerializer(serializers.ModelSerializer):
    # local = serializers.PrimaryKeyRelatedField(queryset=Local.objects.all())
    # local = LocalSerializer()

    class Meta:
        model = Pessoa
        fields = '__all__'

    def is_valid(self, raise_exception=False):
        self.local = self.initial_data.pop('local', None)

        return super(PessoaSerializer, self).is_valid(raise_exception)

    def create(self, validated_data):
        pessoa = super(PessoaSerializer, self).create(validated_data)
        try:
            if self.local is not None:
                local = Local.objects.get(id=self.local['id'])
                pessoa.local = local

            pessoa.save(update_fields=['local'])
            return pessoa
        except Exception as e:
            pessoa.delete()
            return pessoa

    def update(self, pessoa, validated_data):
        pessoa = super(PessoaSerializer, self).update(pessoa, validated_data)
        try:
            if self.local is not None:
                local = Local.objects.get(id=self.local['id'])
                pessoa.local = local
            pessoa.save(update_fields=['local'])
            return pessoa
        except Exception as e:
            pessoa.delete()
            return pessoa

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['local'] = '' if instance.local == "" or instance.local == None else LocalSerializer(instance.local).data

        return response