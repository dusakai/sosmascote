from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework import status

from .models import Pessoa
from .serializers import PessoaSerializer


class ListaPessoa(APIView):
    def get(self, request):
        pessoas = Pessoa.objects.all()
        dado = PessoaSerializer(pessoas, many=True).data
        return Response(dado)

    def post(self, request):
        serializer = PessoaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalhePessoa(APIView):

    def get_object(self, pk):
        try:
            return Pessoa.objects.get(pk=pk)
        except Pessoa.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        pessoa = self.get_object(pk)
        dado = PessoaSerializer(pessoa)
        return Response(dado.data)

    def put(self, request, pk):
        pessoa = self.get_object(pk)
        serializer = PessoaSerializer(pessoa, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        pessoa = self.get_object(pk)
        pessoa.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
