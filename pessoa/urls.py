from django.urls import path, include
from pessoa.views import ListaPessoa, DetalhePessoa

urlpatterns = [
    path('pessoas/', ListaPessoa.as_view(), name="lista_pessoa"),
    path('pessoas/<int:pk>/', DetalhePessoa.as_view(), name="detalhe_pessoa")
]
