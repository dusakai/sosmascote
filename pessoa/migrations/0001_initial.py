# Generated by Django 2.2.6 on 2019-11-06 00:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('local', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pessoa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(blank=True, max_length=50, null=True, verbose_name='nome')),
                ('cpf', models.CharField(db_index=True, max_length=15, unique=True, verbose_name='CPF')),
                ('data_nascimento', models.DateField(blank=True, null=True, verbose_name='data de nascimento')),
                ('fone', models.CharField(blank=True, max_length=11, null=True, verbose_name='fone')),
                ('criado_em', models.DateTimeField(editable=False)),
                ('editado_em', models.DateTimeField(blank=True, null=True, verbose_name='última atualização')),
                ('local', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='local.Local', verbose_name='endereço')),
            ],
            options={
                'verbose_name': 'pessoa',
                'verbose_name_plural': 'pessoas',
                'ordering': ('nome',),
            },
        ),
    ]
