{
"logradouro":"Rua Prof. Celso Ferraz de Camargo",
"numero":"350",
"bairro":"Cidade Universitária",
"complemento":"Bloco B",
"cidade":"Campinas",
"estado": "SP",
"cep":"13083-200",
"geocode":"-22.803997, -47.064340"
}

{
"nome": "Horacio",
"cpf": "414.193.320-15",
"data_nascimento": "1987-10-30",
"fone": "19999666333",
"local": 1,
}


{
"nome": "Horacio",
"cpf": "414.193.320-15",
"data_nascimento": "1985-12-30",
"fone": "19999666333",
"local": {
    "logradouro":"Rua Prof. Celso Ferraz de Camargo",
    "numero":"350",
    "bairro":"Cidade Universitária",
    "complemento":"Bloco B",
    "cidade":"Campinas",
    "estado": "SP",
    "cep":"13083-200",
    "geocode":"-22.803997, -47.064340"
    }
}

{
"nome": "Horacio",
"cpf": "414.193.320-15",
"data_nascimento":"2005-09-21"
}

{
"nome": "Espuleta",
"data_nascimento": "2017-03-06",
"raca": "Braco Alemão",
"cor": "Fígado ruão"
}


{
    "nome": "Foguete",
    "data_nascimento": "2017-03-06",
    "raca": "Braco Alemão",
    "porte": "GRANDE",
    "cor": "Fígado ruão",
    "bravo": false,
    "dono": {
        "id": 2,
        "nome": "Maria"
    }
}


{
"mascote": {"id": 1, "nome": "Espuleta"},
"data_sumico": "2019-10-31T10:33",
"ultimo_local": {"id": 1, "logradouro": "Rua Prof. Celso Ferraz de Camargo"}
}

{
"mascote": {"id": 1, "nome": "Espuleta"},
"mascote_estava_perdido": true,
"data_achado": "2019-11-03T11:11",
"local_achado": {"id": 1, "logradouro": "Rua Prof. Celso Ferraz de Camargo"}
}
